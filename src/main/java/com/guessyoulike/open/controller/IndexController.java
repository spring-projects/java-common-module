package com.guessyoulike.open.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guessyoulike.com
 * @date 2020/6/3 6:58 PM
 */
@Slf4j
@RestController
@RequestMapping(value = "/")
public class IndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping(value = "")
    public String index() {
        return "Guess you like this project";
    }
}
