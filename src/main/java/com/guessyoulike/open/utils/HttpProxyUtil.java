package com.guessyoulike.open.utils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * http工具类
 *
 * @author 贝壳
 * @date 2020/6/10 1:02 PM
 */
public class HttpProxyUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpProxyUtil.class);

    /**
     * Get请求
     * @param url
     * @return
     */
    public static String doGet(String url) {
        try {
            HttpClient httpClient = HttpClients.createDefault();
            HttpGet request = new HttpGet(url);
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity());
            }
        } catch (IOException e) {
            LOGGER.error("HttpProxyUtil.doGet error", e);
        }
        return null;
    }
}
